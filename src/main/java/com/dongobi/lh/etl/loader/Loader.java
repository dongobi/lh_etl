package com.dongobi.lh.etl.loader;

public interface Loader<T> {
    void load(T data);
    void close();
}
