package com.dongobi.lh.etl.loader;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.List;
import java.util.Properties;

public class MongoDBLoader implements Loader<List<Document>> {
    private static final String MONGODB_CONNECTION_STRING_PROP_NAME = "mongodb.url";
    private static final String MONGODB_DATABASE_PROP_NAME = "mongodb.database";
    private static final String MONGODB_CLEAN_PROP_NAME = "mongodb.clean";
    private static final String COLLECTION_NAME = "lh_log_data";

    private final MongoClient client;
    private final MongoCollection<Document> collection;
    public MongoDBLoader(Properties config) {
        String connectionString = config.getProperty(MONGODB_CONNECTION_STRING_PROP_NAME);
        if(connectionString == null) {
            throw new IllegalArgumentException("Cannot find database config");
        }
        ConnectionString cs = new ConnectionString(connectionString);
        String databaseName = cs.getDatabase();
        if(databaseName == null) {
            databaseName = config.getProperty(MONGODB_DATABASE_PROP_NAME);
            if(databaseName == null) {
                throw new IllegalArgumentException("Cannot find database name");
            }
        }
        client = MongoClients.create(cs);
        MongoDatabase database = client.getDatabase(databaseName);
        boolean isClean = Boolean.parseBoolean(config.getProperty(MONGODB_CLEAN_PROP_NAME));
        if(isClean) {
            database.drop();
        }
        collection = database.getCollection(COLLECTION_NAME);
    }

    @Override
    public void load(List<Document> data) {
        collection.insertMany(data);
    }

    @Override
    public void close() {
        client.close();
    }
}
