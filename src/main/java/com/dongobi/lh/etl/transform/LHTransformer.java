package com.dongobi.lh.etl.transform;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.dongobi.lh.etl.processor.LHETLProcessor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LHTransformer implements Transformer<List<String>, List<Document>> {
    private static final Log logger = LogFactory.getLog(LHTransformer.class);
    private static final String OBJECT_FIELD_NAME = "object";
    private static final String COMMAX_DEVICE_FIELD_NAME = "commaxDevice";
    private static final String REG_DATE_FIELD_NAME = "regDate";
    private static final String CENTER_INFO_FIELD_NAME = "centerInfo";
    private static final String CENTER_ID_FIELD_NAME = "centerCd";
    private static final String DONG_FIELD_NAME = "dong";
    private static final String HO_FIELD_NAME = "ho";
    private static final String FAMILY_ID_FIELD_NAME = "family";
    private static final String SUBDEVICE_FIELD_NAME = "subDevice";
    private static final String ROOT_DEVICE_FIELD_NAME = "rootDevice";
    private static final String SORT_FIELD_NAME = "sort";
    private static final String VALUE_FIELD_NAME = "value";

    private Properties config;
    private AmazonS3 s3Client;
    private String bucketName;


    public LHTransformer(Properties config) {
        this.config = config;
    }

    public LHTransformer(AmazonS3 s3Client, String bucketName) {
        this.s3Client = s3Client;
        this.bucketName = bucketName;
    }

    @Override
    public List<Document> transform(List<String> keyList) throws IOException {
        List<Document> transList = new ArrayList<>();

        for(String key:keyList) {
            try {
                S3Object object = s3Client.getObject(bucketName, key);
                S3ObjectInputStream is = object.getObjectContent();
                InputStreamReader inputStreamReader = new InputStreamReader(is);
                Stream<String> streamOfString = new BufferedReader(inputStreamReader).lines();
                Document dataDoc = Document.parse(streamOfString.collect(Collectors.joining()));
                is.close();

                Document objDoc = dataDoc.get(OBJECT_FIELD_NAME, Document.class);

                String regDateStr = dataDoc.getString(REG_DATE_FIELD_NAME);
                LocalDateTime date = LocalDateTime.parse(regDateStr, DateTimeFormatter.ISO_ZONED_DATE_TIME);
                Date regDate = Date.from(date.atZone(ZoneId.systemDefault()).toInstant());

                Document centerInfoDoc = dataDoc.get(CENTER_INFO_FIELD_NAME, Document.class);
                String centerId = centerInfoDoc.getString(CENTER_ID_FIELD_NAME);
                String familyId = centerInfoDoc.getString(DONG_FIELD_NAME) + "-" + centerInfoDoc.getString(HO_FIELD_NAME);

                Document subDevice = objDoc.get(SUBDEVICE_FIELD_NAME, Document.class);

                Document trans = new Document(COMMAX_DEVICE_FIELD_NAME, objDoc.getString(COMMAX_DEVICE_FIELD_NAME));
                trans.put(ROOT_DEVICE_FIELD_NAME, objDoc.getString(ROOT_DEVICE_FIELD_NAME));
                trans.put(CENTER_ID_FIELD_NAME, centerId);
                trans.put(FAMILY_ID_FIELD_NAME, familyId);
                trans.put(REG_DATE_FIELD_NAME, regDate);
                trans.put(SORT_FIELD_NAME, subDevice.getString(SORT_FIELD_NAME));
                trans.put(VALUE_FIELD_NAME, subDevice.getString(VALUE_FIELD_NAME));
                transList.add(trans);
            } catch (Exception e) {
                logger.warn("Object [" + key + "] is broken. Ignored", e);
            }
        }

        return transList;
    }

    @Override
    public void close() throws Exception {

    }
}
