package com.dongobi.lh.etl.transform;

public interface Transformer<E, T> {
    T transform(E data) throws Exception;
    void close() throws Exception;
}
