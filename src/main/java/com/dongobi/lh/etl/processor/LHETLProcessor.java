package com.dongobi.lh.etl.processor;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.dongobi.lh.etl.extract.Extractor;
import com.dongobi.lh.etl.extract.LHExtractor;
import com.dongobi.lh.etl.loader.Loader;
import com.dongobi.lh.etl.loader.MongoDBLoader;
import com.dongobi.lh.etl.transform.LHTransformer;
import com.dongobi.lh.etl.transform.Transformer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.*;

public class LHETLProcessor implements ETLProcessor<String, Document> {
    private static final Log logger = LogFactory.getLog(LHETLProcessor.class);

    private static final String THREAD_NUM_PROP_NAME = "thread.max";
    private static final String EXTRACT_FROM_PROP_NAME = "extract.from";
    private static final String EXTRACT_TO_PROP_NAME = "extract.to";
    private static final String ACCESS_KEY_PROP_NAME = "aws.access.key";
    private static final String ACCESS_SECRET_PROP_NAME = "aws.secret.access.key";
    private static final String S3_END_POINT_PROP_NAME = "aws.s3.endpoint";
    private static final String S3_REGION_PROP_NAME = "aws.s3.region";
    private static final String BUCKET_NAME_PROP_NAME = "aws.s3.bucket.name";
    private static final int LIMITATION = 100;

    private final ThreadPoolExecutor executor;

    private Extractor<List<String>> extractor;
    private BlockingQueue<Runnable> taskQueue = new LinkedBlockingQueue<>();
    private Transformer<List<String>, List<Document>> transformer;
    private Loader<List<Document>> loader;

    public LHETLProcessor(Properties config) {
        int threadNum;
        try {
            threadNum = Integer.parseInt(config.getProperty(THREAD_NUM_PROP_NAME));
        } catch (Exception e) {
            threadNum = Runtime.getRuntime().availableProcessors()*4;
        }

        executor = new ThreadPoolExecutor(threadNum, threadNum, 60000, TimeUnit.MILLISECONDS, taskQueue);
        initModules(config);


    }

    private void initModules(Properties config) {
        String accessKey = config.getProperty(ACCESS_KEY_PROP_NAME);
        String accessSecret = config.getProperty(ACCESS_SECRET_PROP_NAME);
        AWSCredentials credentials = new BasicAWSCredentials(accessKey, accessSecret);

        String endpoint = config.getProperty(S3_END_POINT_PROP_NAME);
        String region = config.getProperty(S3_REGION_PROP_NAME);
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, region)).build();
        String bucketName = config.getProperty(BUCKET_NAME_PROP_NAME);

        String from = config.getProperty(EXTRACT_FROM_PROP_NAME);
        if(from == null) {
            from = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        }

        LocalDate fromDate = LocalDate.parse(from, DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        String to = config.getProperty(EXTRACT_TO_PROP_NAME);

        if(to == null) {
            to = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        }
        LocalDate toDate = LocalDate.parse(to, DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        if(toDate.isBefore(fromDate)) {
            throw new IllegalArgumentException("Invalid date range from [" + fromDate + "] to [" + toDate + "]");
        }
        LocalDate limitDate = toDate.plusDays(1);

        extractor = new LHExtractor(s3Client, bucketName, fromDate, limitDate);
        transformer = new LHTransformer(s3Client, bucketName);
        loader = new MongoDBLoader(config);
    }

    @Override
    public ETLResult doEtl() throws Exception {
        logger.info("Start ETL Process.");
        long extract = 0;
        long load = 0;
        List<Future<ETLResult>> resultList = new ArrayList<>();
        while(extractor.hasNext()) {
            List<String> keyList;
            try {
                keyList = extractor.getNext();
            } catch (Exception e) {
                logger.warn("Cannot get key list. retry.", e);
                continue;
            }
            try {
                while(taskQueue.size() > LIMITATION) {
                    logger.info("Too may remain tasks.[" + taskQueue.size() + "]. Waiting" );
                    Thread.sleep(1000);
                }
                resultList.add(executor.submit(new ETLWorker(keyList, transformer, loader)));
                logger.info("Remain [" + taskQueue.size() + "] tasks.");
            } catch (Exception e) {
                logger.error("Cannot add job to queue", e);
            }

        }

        logger.info("Create Job finished. Waiting result.");
        for(Future<ETLResult> resultFuture:resultList) {
            ETLResult result = resultFuture.get();
            extract += result.getExtractNum();
            load += result.getLoadNum();
        }
        return new ETLResult(extract, load);
    }

    @Override
    public void close() throws Exception{
        executor.shutdown();
        extractor.close();
        transformer.close();
        loader.close();
    }

    private static class ETLWorker implements Callable<ETLResult> {
        private static final Log logger = LogFactory.getLog(ETLWorker.class);
        private final List<String> keyList;
        private final Transformer<List<String>, List<Document>> transformer;
        private final Loader<List<Document>> loader;

        public ETLWorker(List<String> keyList, Transformer<List<String>, List<Document>> transformer, Loader<List<Document>> loader) {
            this.keyList = keyList;
            this.transformer = transformer;
            this.loader = loader;
        }

        @Override
        public ETLResult call() throws Exception {
            long start = System.currentTimeMillis();
            logger.info("Start transforming " + keyList.size() + " data at [" + Thread.currentThread().getName() + "]");
            List<Document> transData = transformer.transform(keyList);

            if(transData != null) {
                loader.load(transData);
            } else {
                transData = new ArrayList<>();
            }
            logger.info("Transforming " + keyList.size() + " data at [" + Thread.currentThread().getName() + "] finished in " + (System.currentTimeMillis() - start) + " ms.");
            return new ETLResult(keyList.size(),transData.size());
        }
    }
}
