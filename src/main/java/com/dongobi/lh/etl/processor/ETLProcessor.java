package com.dongobi.lh.etl.processor;

public interface ETLProcessor <E, T>{
    ETLResult doEtl() throws Exception;
    void close() throws Exception;
}
