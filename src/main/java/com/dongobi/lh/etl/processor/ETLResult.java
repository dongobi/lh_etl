package com.dongobi.lh.etl.processor;

public class ETLResult {
    private final long extractNum;
    private final long loadNum;

    public ETLResult(long extractNum, long loadNum) {
        this.extractNum = extractNum;
        this.loadNum = loadNum;
    }

    public long getExtractNum() {
        return extractNum;
    }

    public long getLoadNum() {
        return loadNum;
    }
}
