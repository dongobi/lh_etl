package com.dongobi.lh.etl.extract;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class LHExtractor implements Extractor<List<String>> {
    private static final Log logger = LogFactory.getLog(LHExtractor.class);
    private static final String DIRECTORY_FORMAT = "yyyy-MM-dd";

    private final String bucketName;
    private final AmazonS3 s3Client;
    private final LocalDate limitDate;

    private LocalDate currentDate;

    private String continuationToken;

    //2021/07/01 Total Count : 3279

    public LHExtractor(AmazonS3 s3Client, String bucketName, LocalDate currentDate, LocalDate limitDate) {
        this.s3Client = s3Client;
        this.bucketName = bucketName;
        this.currentDate = currentDate;
        this.limitDate = limitDate;
    }

    @Override
    public synchronized boolean hasNext() {
        return currentDate.isBefore(limitDate);
    }

    @Override
    public synchronized List<String> getNext() {
        List<S3ObjectSummary> summaries = getList();
        List<String> keyList = new ArrayList<>(summaries.size());
        for(S3ObjectSummary summary:summaries) {
            keyList.add(summary.getKey());
        }
        return keyList;
    }

    private synchronized List<S3ObjectSummary> getList() {
        String dirName = currentDate.format(DateTimeFormatter.ofPattern(DIRECTORY_FORMAT));
        ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucketName).withMaxKeys(1000).withPrefix("log/" + dirName);
        if(continuationToken != null) {
            req.setContinuationToken(continuationToken);
        }
        ListObjectsV2Result result;
        result = s3Client.listObjectsV2(req);
        continuationToken = result.getNextContinuationToken();
        List<S3ObjectSummary> list = result.getObjectSummaries();
        logger.info("Create transform job from [" + dirName + "] with " + list.size() + " data");
        if(!result.isTruncated()) {
            currentDate = currentDate.plusDays(1);
            continuationToken = null;
            logger.info("[" + dirName + "] processing is finished. Move to next dir");
        }
        return list;
    }

    @Override
    public void close() throws Exception {
        s3Client.shutdown();
    }
}
