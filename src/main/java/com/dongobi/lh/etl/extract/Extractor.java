package com.dongobi.lh.etl.extract;


import java.io.IOException;
import java.io.InputStream;

public interface Extractor<E> {
    boolean hasNext() throws IOException;
    E getNext() throws IOException;
    void close() throws Exception;
}
