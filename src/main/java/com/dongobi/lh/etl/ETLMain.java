package com.dongobi.lh.etl;

import com.dongobi.lh.etl.processor.ETLResult;
import com.dongobi.lh.etl.processor.LHETLProcessor;
import org.apache.commons.cli.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.*;
import java.util.Properties;

//> Sys.setenv("AWS_ACCESS_KEY_ID" = "B9CCEB079EE0493BB257",
//+            "AWS_SECRET_ACCESS_KEY" = "D9C8EDA3F549F10AFDE1C393983BA90AB2964E06",
//+            "AWS_S3_ENDPOINT" = "object.gov-ncloudstorage.com",
//+            "AWS_DEFAULT_REGION" = "kr")
public class ETLMain {
    private static final Log logger = LogFactory.getLog(ETLMain.class);
    private static final String AWS_ACCESS_KEY_ID = "B9CCEB079EE0493BB257";
    private static final String AWS_SECRET_ACCESS_KEY = "D9C8EDA3F549F10AFDE1C393983BA90AB2964E06";
    private static final String AWS_S3_ENDPOINT = "https://kr.object.gov-ncloudstorage.com";

    public static void main(String[] args) throws Exception {
        Option help = Option.builder().argName("h").longOpt("help").desc("Print usage").build();
        Option config = Option.builder().argName("c").longOpt("config").desc("Config file path").hasArg(true).build();
        Options options = new Options();
        options.addOption(help);
        options.addOption(config);

        CommandLineParser parser = new DefaultParser();
        CommandLine line;
        try {
            line = parser.parse(options, args);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "etl.sh", options );
            return;
        }

        if(line.hasOption("help")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "etl.sh", options );
            return;
        }

        Properties properties = new Properties();
        InputStream defaultPropStream = ETLMain.class.getClassLoader().getResourceAsStream("prop.properties");
        if(defaultPropStream != null) {
            try {
                properties.load(defaultPropStream);
                defaultPropStream.close();
            } catch (IOException e) {
                logger.error(e.getMessage());
                return;
            }
        }

        if(line.hasOption("config")) {
            try {
                String configFilePath = line.getOptionValue("config");
                FileInputStream fis = new FileInputStream(configFilePath);
                properties.load(fis);
                fis.close();
            } catch (IOException e) {
                logger.error("Cannot find or invalid config file [" + line.getOptionValue("config") + "].");
                return;
            }
        }

        try {
            LHETLProcessor etlProcessor = new LHETLProcessor(properties);
            long startTime = System.currentTimeMillis();
            ETLResult result = etlProcessor.doEtl();
            logger.info("Finished in " + (System.currentTimeMillis() - startTime) + "ms");
            logger.info("Extract : [" + result.getExtractNum() + "]\tLoad: [" + result.getLoadNum() +"]\tIgnored: [" + (result.getExtractNum() - result.getLoadNum()) + "]");
            etlProcessor.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }


/*        long start = System.currentTimeMillis();
        AWSCredentials credentials = new BasicAWSCredentials(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY);
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(AWS_S3_ENDPOINT, "kr-standard")).build();


        ListObjectsV2Request req = new ListObjectsV2Request().withBucketName("iot-device-values-history").withMaxKeys(10000).withPrefix("log/2021-07-01/");
        ListObjectsV2Result result;
        long count = 0;
        do {
            result = s3Client.listObjectsV2(req);

            for (S3ObjectSummary objectSummary : result.getObjectSummaries()) {
                S3Object object = s3Client.getObject("iot-device-values-history", objectSummary.getKey());
                S3ObjectInputStream is = object.getObjectContent();
                InputStreamReader inputStreamReader = new InputStreamReader(is);
                Stream<String> streamOfString= new BufferedReader(inputStreamReader).lines();
                String streamToString = streamOfString.collect(Collectors.joining());
                System.out.println(streamToString);
                count++;
            }
            System.out.println("Current Count :" + count);
            // If there are more than maxKeys keys in the bucket, get a continuation token
            // and list the next objects.
            String token = result.getNextContinuationToken();
            System.out.println("Next Continuation Token: " + token);
            req.setContinuationToken(token);
        } while (result.isTruncated());

        System.out.println(System.currentTimeMillis() - start + " Total Count : " + count);*/
    }
}
