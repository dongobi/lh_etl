package com.dongobi.lh.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamUtil {
    public static String streamToString(InputStream is) {
        InputStreamReader inputStreamReader = new InputStreamReader(is);
        Stream<String> streamOfString= new BufferedReader(inputStreamReader).lines();
        return streamOfString.collect(Collectors.joining());
    }
}
